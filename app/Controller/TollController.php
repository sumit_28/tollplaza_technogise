<?php

namespace App\Controller;

use \App\Model\IVehicle;
use \App\Model\TollTax;

/*
 * TollCalculator.php
 * This controller is used to communicate view with models 
 * create by : Sumit [25-Mar-2015 10:00am] 
 */

class TollController {

    // to store state where toll plaza belongs
    private $tollPlazaState = "MP";
    // To check if vehicle is on government duty
    private $onGovDuty = false;

    function __construct(IVehicle $vehicle) {
        $this->vehicle = $vehicle;
    }

    public function isLeviedTollTax() {
        // Check 
        // If it is of same state then there will be no tax
        // If vehicle is of different state then pay tax
        // If vehicle is of different state and is on government duty then there will be no tax
        if ($this->vehicleIsOfDifferentState() and ! $this->onGovernmentDuty()) {
            return true;
        }
    }

    // To check if vehicle belongs to different state
    public function vehicleIsOfDifferentState() {
        if (strToUpper($this->vehicle->state()) != $this->tollPlazaState) {
            return true;
        }
        return false;
    }

    // To check if vehicle is on government duty

    public function onGovernmentDuty() {
        return $this->onGovDuty;
    }

    // To set vehicle is on government duty

    public function setOnGovernmentDuty() {
        $this->onGovDuty = true;
    }

    // To calculate the tax
    public function calculateTax() {
        $tax = new TollTax($this->vehicle);
        return $tax->calculateTax();
    }

    // To set toll state

    public function setTollState($state) {
        $this->tollPlazaState = $state;
    }

}

?>
