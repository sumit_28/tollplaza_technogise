<?php

namespace App\Model;

use \App\Model\IVehicle;

class TollTax {

    // Tax rates according to wheels and axle type
    private $taxOn2Wheeler = 20;
    private $taxOn3Wheeler = 50;
    private $taxOn4WheelerNonTransport = 100;
    private $taxOn4WheelerTransport = 200;
    private $taxOn6Wheeler = 500;
    private $baseAxle = 500;
    private $additionalAxle = 100;

    function __construct(IVehicle $vehicle) {
        $this->vehicle = $vehicle;
    }

    /**
     * Calculate toll tax according to vehicle type, wheels and axle
     *
     * @return String
     */
    public function calculateTax() {

        switch ($this->vehicle->getWheels()) {
            case "2":
                return $this->taxForTwoWheeler();
                break;
            case "3":
                return $this->taxForThreeWheeler();
                break;
            case "4":
                return $this->taxForFourWheeler();
                break;
            case "6":
                return $this->taxForSixWheeler();
                break;
            default:
                return "Invalid Wheel type";
        }
    }

    // To calculate tax for 2 wheeler
    protected function taxForTwoWheeler() {
        return $this->taxOn2Wheeler;
    }

    // To calculate tax for 3 wheeler

    protected function taxForThreeWheeler() {
        return $this->taxOn3Wheeler;
    }

    // To calculate tax for 4 wheeler

    protected function taxForFourWheeler() {
        // check number of axle
        if ($this->vehicle->getAxle() > 1) {
            return $this->taxForAxleVehicle();
        }
        // Check vehicle type
        if ($this->vehicle->getType() == 1) {
            //  Vehicle is of type Transport
            return $this->taxOn4WheelerNonTransport;
        }

        if ($this->vehicle->getType() == 2) {
            //  Vehicle is of type Non Transport
            return $this->taxOn4WheelerTransport;
        }
    }

    // To calculate tax for 6 wheeler

    protected function taxForSixWheeler() {
        // check number of axle
        if ($this->vehicle->getAxle() > 1) {
            return $this->taxForAxleVehicle();
        }
        return $this->taxOn6Wheeler;
    }

    // To calculate tax for vehicle having more than 1 axle

    protected function taxForAxleVehicle() {
        $additionalAmount = 0;
        $numOfAxle = $this->vehicle->getAxle();
        while ($numOfAxle >= 1) {
            $additionalAmount = $additionalAmount + $this->additionalAxle;
            $numOfAxle--;
        }
        return $this->baseAxle + $additionalAmount;
    }

}

?>
