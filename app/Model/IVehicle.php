<?php

namespace App\Model;

Interface IVehicle {

    function getRegisrationNumber();

    function getType();

    function getWheels();

    function getAxle();

    function setRegisrationNumber($regisrationNumber);

    function setType($type);

    function setWheels($wheels);

    function setAxle($axle);

    function state();
}

?>
