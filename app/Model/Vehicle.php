<?php

namespace App\Model;

use \App\Model\IVehicle;

class Vehicle implements IVehicle {

    private $regisrationNumber;
    private $type;
    private $wheels;
    private $axle;

    function getRegisrationNumber() {
        return $this->regisrationNumber;
    }

    function getType() {
        return $this->type;
    }

    function getWheels() {
        return $this->wheels;
    }

    function getAxle() {
        return $this->axle;
    }

    function setRegisrationNumber($regisrationNumber) {
        $this->regisrationNumber = $regisrationNumber;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setWheels($wheels) {
        $this->wheels = $wheels;
    }

    function setAxle($axle) {
        $this->axle = $axle;
    }

    // To get state from the registration number, extract first two character from string
    function state() {
        return substr($this->regisrationNumber, 0, 2);
    }

}

?>
