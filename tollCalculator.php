<?php
require('vendor/autoload.php');

use \App\Controller\TollController;
use \App\Model\Vehicle;

$taxAmount = 0;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $errors = [];
    // Check all input fields
    $data = $_POST;
    if (empty($data['registration_number'])) {
        $errors[] = "Please enter registration number";
    } elseif (!preg_match("/[A-Z]{2}[0-9]{1,2}(?:[A-Z])?(?:[A-Z]*)?[0-9]{4}/i", $data['registration_number'], $match)) {
        $errors[] = "Registration number should be in proper format (Ex: MH14A1234)";
    }

    if (empty($data['type']) and $data['type'] != 1 and $data['type'] != 2) {
        $errors[] = "Invalid Vehicle type. Please select vehicle type properly.";
    }

    if (empty($data['wheels'])) {
        $errors[] = "Please select number of wheels properly.";
    }

    // If there is no error calculate the tax
    if (sizeof($errors) == 0) {
        $vehicle = new Vehicle();
        // Set Vehicle fields
        $vehicle->setRegisrationNumber($data['registration_number']);
        $vehicle->setType($data['type']);
        $vehicle->setWheels($data['wheels']);
        $vehicle->setAxle($data['axle']);


        $tollController = new TollController($vehicle);
        
        // Set State to which toll belongs
        $tollController->setTollState('MP');

        //Check if vehicle is on government duty
        if (isset($data['onGovDuty']) and $data['onGovDuty'] == "true") {
            $tollController->setOnGovernmentDuty();
        }

        //Check if vehicle should pay tax or not
        if ($tollController->isLeviedTollTax()) {
            $taxAmount = $tollController->calculateTax();
        } else {
            $noTaxMessage =  "Vehicle is exempted from toll tax <br>";
            $noTaxMessage .=   "Reason : ";
            if (!$tollController->vehicleIsOfDifferentState()) {
                $noTaxMessage .=   "Vehicle is from same state. ";
            }

            if ($tollController->onGovernmentDuty()) {
                $noTaxMessage .=   "Vehicle is on Government Duty. ";
            }
        }
    }
} else {
    exit("Method not allowed");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Toll Plaza Output</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <?php foreach ($errors as $error) { ?>
                <div class="alert alert-danger">
                    <?php echo $error; ?>            
                </div>
            <?php } ?>
        </div>
        <div class="container-fluid bg-grey">
            <h2 class="text-center">Toll Tax Receipt</h2>
            <div class="row">
                <div class="col-sm-5">
                    <p>Vehicle Registration Number : <?php echo $data['registration_number']; ?></p>
                    <p> Vehicle Type : <?php echo $data['type'] == 1 ? "Non Transport" : "Transport"; ?></p>
                    <p> Number of Wheels : <?php echo $data['wheels']; ?></p>
                    <?php if($data['axle']>1){ ?>
                        <p> Number of Axle : <?php echo $data['axle']; ?></p>
                        <p> Tax :  500(Base Amount) + <?php echo $data['axle']; ?> (Number of axles) x 100 </p>
                    <?php }?>
                    <p>Total Tax to be paid :  <?php echo $taxAmount; ?>/-</p>
                    <p><?php echo $noTaxMessage; ?></p>
                </div>

            </div>
        </div>

    </body>
</html>