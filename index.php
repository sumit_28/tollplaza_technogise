<?php
$errors = [];
// Check all input fields
if (isset($_POST) and $_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST;
    if (empty($data['registration_number'])) {
        $errors[] = "Please enter registration number";
    } elseif (!preg_match("/[A-Z]{2}[0-9]{1,2}(?:[A-Z])?(?:[A-Z]*)?[0-9]{4}/i", $data['registration_number'], $match)) {
        $errors[] = "Registration number should be in proper format (Ex: MH14A1234)";
    }

    if (empty($data['type']) and $data['type'] != 1 and $data['type'] != 2) {
        $errors[] = "Invalid Vehicle type. Please select vehicle type properly.";
    }

    if (empty($data['wheels'])) {
        $errors[] = "Please select number of wheels properly.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Toll Plaza</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h2>Welcome to Toll Plaza</h2>
            <div class="container">
                <?php
                if(isset($errors) and sizeof($errors)>0) {
                    foreach ($errors as $error) {
                        ?>
                        <div class="alert alert-danger">
                        <?php echo $error; ?>            
                        </div>
                    <?php }
                }
                ?>
            </div>

            <form method="post" action="index.php" id="tollForm">
                <div class="form-group">
                    <label for="registration_number">Vehicle Registration Number:</label>
                    <input type="text" class="form-control" id="registration_number" name="registration_number" placeholder="Enter Registration Number" value="<?php echo isset($data['registration_number']) ? $data['registration_number'] : "" ?>">
                </div>
                <div class="form-group">
                    <label for="type">Type:</label>
                    <label class="radio-inline">
                        <input type="radio" name="type" value="1" <?php echo (isset($data['type']) and $data['type'] == 1) ? "checked" : "" ?>>Non Transport
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="type" value="2" <?php echo (isset($data['type']) and $data['type'] == 2) ? "checked" : "" ?>> Transport
                    </label>

                </div>
                <div class="form-group">
                    <label for="wheels">Number of wheels:</label>
                    <select class="form-control" id="wheels" name="wheels" onchange="displayAxleDiv();" onselect="displayAxleDiv();">
                        <option value="">-Select-</option>
                        <option value="2" <?php echo (isset($data['wheels']) and $data['wheels'] == 2) ? "selected" : "" ?>>2</option>
                        <option value="3" <?php echo (isset($data['wheels']) and $data['wheels'] == 3) ? "selected" : "" ?>>3</option>
                        <option value="4" <?php echo (isset($data['wheels']) and $data['wheels'] == 4) ? "selected" : "" ?>>4</option>
                        <option value="6" <?php echo (isset($data['wheels']) and $data['wheels'] == 6) ? "selected" : "" ?>>6</option>
                    </select>                </div>

                <div class="form-group" style="display:none;" id="axleDiv">
                    <label for="axle">Number of axle:</label>
                    <input type="number" class="form-control" id="axle" name="axle" placeholder="Enter number of axle" value="<?php echo isset($data['axle']) ? $data['axle'] : "1" ?>">
                </div>

                <div class="checkbox">
                    <label><input type="checkbox" value="true" name="onGovDuty" <?php echo (isset($data['onGovDuty']) and $data['onGovDuty'] == true) ? "checked" : "" ?>>On Government Duty</label>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <script type="text/javascript">
            displayAxleDiv();
            function displayAxleDiv() {
                if ($('#wheels').val() >= 4) {
                    $('#axleDiv').show();
                } else {
                    $('#axleDiv').hide();

                }
            }
        </script>

        <?php
        if (isset($_POST) and $_SERVER['REQUEST_METHOD'] === 'POST' and sizeof($errors) == 0) {
            ?>
            <script>
                form = document.getElementById("tollForm");
                form.action = 'tollCalculator.php';
                form.submit();
            </script>
            <?php
        }
        ?>
    </body>
</html>